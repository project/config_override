<?php

namespace Drupal\config_override\Tests\Unit {

  use Drupal\Component\FileCache\FileCacheFactory;
  use Drupal\Component\Serialization\Yaml;
  use Drupal\config_override\SiteConfigOverrides;
  use Drupal\Core\Cache\CacheBackendInterface;
  use org\bovigo\vfs\vfsStream;
  use PHPUnit\Framework\TestCase;
  use Prophecy\PhpUnit\ProphecyTrait;

  /**
   * @coversDefaultClass \Drupal\config_override\SiteConfigOverrides
   * @group config_override
   */
  class SiteConfigOverridesTest extends TestCase {
    use ProphecyTrait;

    /**
     * Tests site level overrides.
     */
    public function testSiteOverride() {
      $cache_backend = $this->prophesize(CacheBackendInterface::class);
      $site_override = new SiteConfigOverrides('vfs://drupal', $cache_backend->reveal());

      // Set file cache factory prefix to prevent the below exception.
      // InvalidArgumentException: Required prefix configuration is missing.
      FileCacheFactory::setPrefix('test');

      vfsStream::setup('drupal');
      vfsStream::create([
        'sites' => [
          'default' => [
            'config' => [
              'override' => [
                'system.site.yml' => Yaml::encode(['name' => 'Hey jude']),
              ],
            ],
          ],
        ],
      ]);

      $result = $site_override->loadOverrides(['system.site']);
      $this->assertEquals([
        'system.site' => [
          'name' => 'Hey jude',
        ],
      ], $result);
    }

  }

}
