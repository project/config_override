<?php

namespace Drupal\config_override\Tests\Unit;

use Drupal\Component\FileCache\FileCacheFactory;
use Drupal\Component\Serialization\Yaml;
use Drupal\config_override\ModuleConfigOverrides;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ModuleHandlerInterface;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\config_override\ModuleConfigOverrides
 * @group config_override
 */
class ModuleConfigOverridesTest extends TestCase {
  use ProphecyTrait;

  /**
   * Tests the module config overrides.
   */
  public function testModuleOverrides() {
    $cache_backend = $this->prophesize(CacheBackendInterface::class);
    $module_handler = $this->prophesize(ModuleHandlerInterface::class);

    // Set file cache factory prefix to prevent the below exception.
    // InvalidArgumentException: Required prefix configuration is missing.
    FileCacheFactory::setPrefix('test');

    $module_info = <<<MODULE_INFO
name: Virtual module
type: module
description: 'Description.'
core_version_requirement: '*'
MODULE_INFO;

    vfsStream::setup('drupal');
    vfsStream::create([
      'modules' => [
        'module_a' => [
          'module_a.info.yml' => $module_info,
          'config' => [
            'override' => [
              'system.site.yml' => Yaml::encode([
                'name' => 'Hey jude',
              ]),
            ],
          ],
        ],
        'module_b' => [
          'module_b.info.yml' => $module_info,
          'config' => [
            'override' => [
              'system.site.yml' => Yaml::encode([
                'slogan' => 'Muh',
              ]),
            ],
          ],
        ],
      ],
    ]);

    $extension_a = new Extension('vfs://drupal', 'module', 'modules/module_a/module_a.info.yml');
    $extension_b = new Extension('vfs://drupal', 'module', 'modules/module_b/module_b.info.yml');

    $module_handler->getModuleList()->willReturn([
      'module_a' => $extension_a,
      'module_b' => $extension_b,
    ]);
    $module_overrides = new ModuleConfigOverrides('vfs://drupal', $module_handler->reveal(), $cache_backend->reveal());

    $expected = [
      'system.site' => [
        'name' => 'Hey jude',
        'slogan' => 'Muh',
      ],
    ];
    $result = $module_overrides->loadOverrides(['system.site']);

    $this->assertEquals($expected, $result);
  }

}
